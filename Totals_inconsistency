# -*- coding: utf-8 -*-
"""
Created on Tue Apr 10 20:58:00 2018

@author: David Ardila
"""

import pandas as pd

VALUE_VARLIST = ["capital", "greatest number of employees in the year", "avg number of employees, males",
"avg number of employees, females","avg number of employees, children", "number of hours of labor, may-nov",
"number of hours of labor, nov-may","avg daily wage, ordinary laborer", "total annual wages",
"materials, values","production, values"]

def partition_table(df):
    """
    In each table, we have information on hundreds of pages of manuscripts. 
    This function is used to generate a list, which record the starting line 
    and the end line of a single page.
    
    Input:
        - df: (pandas DataFrame) pandas DataFrame containing the dataset information
    Return:
        - file_partition: (list of tuples) ducumenting the starting and end line
                          of a certain manuscript page
    """
    
    # Initialize variables
    current_filename = df["file_name"][0]
    start_line = 0
    end_line = 0

    file_partition = []
    for index, item in df.iterrows():
        if item["file_name"] != current_filename:        
            if pd.isnull(item["file_name"]):
                continue
            else:
                """
                Hardcode end_line in this way assumes that there exists a 
                one-line gap between each manuscript page
                """
                end_line = index - 2
                file_partition.append((start_line, end_line))
                current_filename = item["file_name"]
                start_line = index
    
    # print(file_partition)
           
    return file_partition

def check_single_establishment_total(linenum, var):
    """
    Check if one line is the "apparent total" of a single establishment
    
    Input:
        - linenum: (integer) the number of the line
        - var: (string) column name of the table
        
    Return:
        - True or False. True if it is a single-establishment total, False otherwise
    """
    
    apparent_total = df[var][linenum]

    firm_number = df["firm_number"][linenum - 1]
    
    temp_sum = 0
    temp_index = linenum - 1
    
    while True:
        if firm_number != df["firm_number"][temp_index]:
            break
    
        if pd.isnull(df[var][temp_index]):
            temp_index -= 1
            continue
        else:
            if isinstance(df[var][temp_index], (int, float)):
                temp_sum += df[var][temp_index]
                temp_index -= 1
            else:
                temp_index -= 1
                continue
    if temp_sum == apparent_total:
        return True
    else:
        return False
                                        
def correct_totals_error(file_partition):
    """
    This function is to correct the label errors in "totals" column. In the excel
    file, there are many label errors in the "totals" column. In fact, they label
    too many "establishment total" as "page total".
    
    Inputs:
        - file_partition: (list of tuples) document the starting and end line 
                                           of a single manuscript page
    Returns:
        - No returns, direct change the original DataFrame ("df" below)
    """

    global corrected_totals_error
    global error_esttotal_cell
    global solved_esttotal_error
    
    for line_range in file_partition:
        start_line, end_line = line_range[0], line_range[1]
        
        # Slice a small DataFrame which contains information on a single page
        temp_df = df.loc[start_line: end_line + 1, :]
        
        # The total number of "2" in the "totals" column
        count_2 = temp_df[temp_df["totals"] == 2].shape[0]
        
        if count_2 > 1:
            location_list = temp_df[temp_df["totals"] == 2].index.tolist()
            len_list = len(location_list)
            
            for index, item in enumerate(location_list):
                # Change all but the last "2" to "1", since they're unable to be page total
                if index != len_list - 1:
                    df.loc[item, "totals"] = 1
                    corrected_totals_error += 1
    
                if index == len_list - 1:
                    """
                    Things will be more comlicated when it comes to the last "2"
                    since it can also be a "establishment total" or "page total".
                    
                    My strategy here is to test whether it can be a "establishment total"
                    (eith multiple-establishment or single establishment). If it's not,
                    treat it as page total
                    """
                    
                    last_cutoff = location_list[-2]
                    
                    for var in VALUE_VARLIST:
                        if pd.isnull(df[var][item]):
                            continue              
                        else:
                            error_esttotal_cell += 1
                            
                            small_df = df.loc[last_cutoff + 1: item - 1, :]
                            
                            try:
                                computed_total = small_df[var].sum()
                            except TypeError:
                                small_df[var] = small_df[var].apply(pd.to_numeric, errors = 'coerce')
                                computed_total = small_df[var].sum()  
                            
                            if df[var][item] == computed_total:
                                # This is the case where it is a "multiple-establishments" total
                                df.loc[item, "totals"] = 1
                                solved_esttotal_error += 1
                                # print(item, last_cutoff, computed_total, df[var][item])
                                # print("This is the multiple-establishmen total")
                            else:
                                # Further check if it's single-establishment total
                                True_single = check_single_establishment_total(item, var)      
                                if True_single:
                                    df.loc[item, "totals"] = 1
                                    solved_esttotal_error += 1
                                    # print("This, is single establishment total") 
                                    
    print("Done with Checking totals error")
    
def check_establishment_total(file_partition):
    """
    This function is to check whether the establishment total is computed correctly.
    The establishment total, in most cases, are computed based on a multiple-establishment
    condition, but in some cases, it is a single-establishment total. This function
    can handle these two cases.
    
    Inputs:
        - file_partition: (list of tuples) document the starting and end line 
                                           of a single manuscript page
    Returns:
        - error_list: (list of tuples) documenting the still-existing errors.
                      Namely, the computed total cannot match the apparent total
                      (either multiple-establishment case or single-establishment
                      case)
    """      
    global corrected_totals_error
    global error_esttotal_cell
    global solved_esttotal_error
      
    error_list = []     
    for line_range in file_partition:
        start_line, end_line = line_range[0], line_range[1]
        
        # Slice a small DataFrame which contains information on a single page
        temp_df = df.loc[start_line: end_line + 1, :]
        
        # The total number of "1" in the "totals" column
        count_1 = temp_df[temp_df["totals"] == 1].shape[0]
        
        if count_1 == 0:
            continue
        
        location_list = temp_df[temp_df["totals"] == 1].index.tolist()
        
        for index, endline in enumerate(location_list):
            if index == 0:
                start = start_line
            else:
                start = location_list[index - 1] + 1
            
            end = endline - 1
            
            # Slice a smaller DataFrame
            small_df = df.loc[start: end, :]
            
            
            for var in VALUE_VARLIST:
                if pd.isnull(df[var][endline]):
                    continue
                else:
                    error_esttotal_cell += 1

                    # Check if the value will match based on a multiple-establishment manner
                    try:
                        computed_total = small_df[var].sum()
                    except TypeError:
                        small_df[var] = small_df[var].apply(pd.to_numeric, errors = 'coerce')
                        computed_total = small_df[var].sum() 
                        
                    apparent_total = df[var][endline]
                    
                    if computed_total == apparent_total:
                        solved_esttotal_error += 1
                    else:
                        # Further check if it's single firm summation
                        True_single = check_single_establishment_total(endline, var)
                        
                        if True_single:
                            solved_esttotal_error += 1
                            continue
                            # print("This, is single firm summation")     
                        else:
                            # print("Error Occurs!")
                            # print(computed_total, apparent_total, var, item)
                            
                            # We nned endline + 2 to match the excel line number
                            error_list.append((endline + 2, var))
                          
    return error_list

def check_page_total(df, file_partition):
    """
    This is a function cheking the error regarding page total
    """
    
    error_count_dict = {}
    error_list = []   
    
    for line_range in file_partition:
        start_line, end_line = line_range[0], line_range[1]
        
        # Slice a small DataFrame which contains information on a single page
        temp_df = df.loc[start_line: end_line + 1, :]
        
        # The total number of "2" in the "totals" column
        count_2 = temp_df[temp_df["totals"] == 2].shape[0]
        
        if count_2 == 0:
            continue
        
        location_list = temp_df[temp_df["totals"] == 2].index.tolist()

        pagesum_loc = location_list[0]
        
        for var in VALUE_VARLIST:
            if pd.isnull(df[var][pagesum_loc]):
                continue              
            else:
                if var not in error_count_dict:
                    error_count_dict[var] = [0, 0, 0]
                    
                error_count_dict[var][0] += 1
                error_count_dict[var][2] += temp_df[var].count() - 1
                
                small_df = temp_df[temp_df["totals"] == 1]
                
                try:
                    computed_total = temp_df[var].sum() - small_df[var].sum() - df[var][pagesum_loc]
                except TypeError:
                    temp_df[var] = temp_df[var].apply(pd.to_numeric, errors = 'coerce')
                    small_df[var] = small_df[var].apply(pd.to_numeric, errors = 'coerce')
                    computed_total = temp_df[var].sum() - small_df[var].sum()       
                    
                if computed_total != df[var][pagesum_loc]:
                    error_count_dict[var][1] += 1
                    error_list.append((pagesum_loc + 2, var))
                    
    return error_list

def output_error_position(error_list, output_file):
    error_df = pd.DataFrame(error_list, columns=['line', 'var'])
    
    column_names = {"capital":'J', "greatest number of employees in the year":'K', "avg number of employees, males":'L',
"avg number of employees, females":'M',"avg number of employees, children":'N', "number of hours of labor, may-nov":'O',
"number of hours of labor, nov-may":'P',"avg daily wage, ordinary laborer":'Q', "total annual wages":'S',
"materials, values":'Y',"production, values":'Z'}
    
    error_df = error_df.replace({'var': column_names})
    error_df = error_df['var'] + error_df['line'].astype(str)
    
    error_df.to_csv(output_file, sep=',', encoding = 'utf-8', index = False)
    
if __name__ == "__main__":  
    # filename = "D:/Dropbox/RA_Hornbeck/Data_check/Output_IL1860.xlsx"
    filename = r"C:\Users\D\Dropbox\DDD Output - firm_level\checks\1880\to_prepare\KY1880\Output\Output_KY1880_S3.xlsx"
    
    df = pd.read_excel(filename)

    # Initialize some global variables to document the error rate    
    global corrected_totals_error
    global error_esttotal_cell
    global solved_esttotal_error
    
    corrected_totals_error = 0
    error_esttotal_cell = 0
    solved_esttotal_error = 0

    file_partition = partition_table(df)
    correct_totals_error(file_partition)
    est_error_list = check_establishment_total(file_partition)
    page_error_list = check_page_total(df, file_partition)
    
    """
    print("Total error cells: ", error_esttotal_cell + corrected_totals_error)
    print("Total automatically corrected label error: ", corrected_totals_error)
    print("Total automatically corrected cell error: ", solved_esttotal_error)
    print("Reduce labor by: ", (solved_esttotal_error + corrected_totals_error) \
                               /(error_esttotal_cell + corrected_totals_error) * 100, \
                               "%")          
    """
    
    output_file = r"C:\Users\D\Dropbox\DDD Output - firm_level\checks\1880\to_prepare\KY1880\Output\Output_KY1880_S3_est.txt"
    output_error_position(est_error_list, output_file)
    
    output_file = r"C:\Users\D\Dropbox\DDD Output - firm_level\checks\1880\to_prepare\KY1880\Output\Output_KY1880_S3_page.txt"
    output_error_position(page_error_list, output_file)